""" Business logic module """

__all__ = ["Logic", "LogicException"]
__author__ = 'Damian Giebas'
__copyright___ = "Copyright (c) 2019 Damian Giebas"
__email__ = 'damian.giebas@gmail.com'
__name__ = "business_logic"
__version__ = '1.1'

import numexpr


class LogicException(BaseException):
    """ Logic Expression class """
    pass


class Logic:
    """ Business logic class """
    def __init__(self):
        self.__history = []
        self.__compute_is_last_action = False

    def calculate(self, expression: str) -> str:
        """ Method to calculate expression """
        try:
            result = numexpr.evaluate(expression)
            self.__history.append(expression)
            self.__history.append(str(result))
            self.__compute_is_last_action = True
            return result
        except ZeroDivisionError:
            raise LogicException("Cannot divide by 0!")
        except (ValueError, KeyError):
            raise LogicException("Incorrect expression!")
        except SyntaxError as se:
            raise LogicException(f"Incorrect expression: {se.text}!")

    def get_last_expression(self) -> str:
        """ Method to get last expression """
        try:
            if self.__compute_is_last_action:
                self.__history.pop()
                self.__compute_is_last_action = False
            return self.__history.pop()
        except IndexError:
            raise LogicException("History is empty!")
