__author__ = 'Damian Giebas'
__copyright___ = "Copyright (c) 2019 Damian Giebas"
__email__ = 'damian.giebas@gmail.com'
__name__ = "business_logic_tests"
__version__ = '1.1'

from pycalc.logic import Logic, LogicException
import unittest


class LogicTest(unittest.TestCase):
    def setUp(self):
        self.logic = Logic()

    def test_calculate_raise_error_on_text(self):
        test_text = "7 * a"
        with self.assertRaises(LogicException) as le:
            self.logic.calculate(test_text)
        self.assertEqual("Incorrect expression!", str(le.exception))

    def test_calculate_raise_error_on_text_with_source_code(self):
        test_text = "import pdb; pdb.set_trace()"
        with self.assertRaises(LogicException) as le:
            self.logic.calculate(test_text)
        self.assertEqual(f"Incorrect expression: {test_text}!", str(le.exception))

    def test_calculate_raise_error_on_dividing_by_0(self):
        test_text = "1 / 0"
        with self.assertRaises(LogicException) as le:
            self.logic.calculate(test_text)
        self.assertEqual("Cannot divide by 0!", str(le.exception))

    def test_calculate_multiplication_operation_by_0(self):
        test_text = "1 * 0"
        result = self.logic.calculate(test_text)
        self.assertEqual(0, result)

    def test_calculate_multiplication_operation_by_huge_number(self):
        test_text = "7*9999999999"
        result = self.logic.calculate(test_text)
        self.assertEqual(69999999993, result)

    def test_calculate_division_operation_by_1(self):
        test_text = "2 / 1"
        result = self.logic.calculate(test_text)
        self.assertEqual(2, result)

    def test_calculate_division_operation_by_2(self):
        test_text = "7 / 2"
        result = self.logic.calculate(test_text)
        self.assertEqual(3.5, result)

    def test_calculate_division_operation_to_btain_1(self):
        test_text = "347634564576 / 347634564576"
        result = self.logic.calculate(test_text)
        self.assertEqual(1, result)

    def test_calculate_adding_0(self):
        test_text = "456 + 0"
        result = self.logic.calculate(test_text)
        self.assertEqual(456, result)

    def test_calculate_adding_1(self):
        test_text = "347634564576 + 347634564576"
        result = self.logic.calculate(test_text)
        self.assertEqual(347634564576 * 2, result)

    def test_calculate_adding_minus_1(self):
        test_text = "9 + -1"
        result = self.logic.calculate(test_text)
        self.assertEqual(8, result)

    def test_calculate_subtraction_minus_0(self):
        test_text = "9 - 0"
        result = self.logic.calculate(test_text)
        self.assertEqual(9, result)

    def test_calculate_subtraction_minus_1(self):
        test_text = "9 - 1"
        result = self.logic.calculate(test_text)
        self.assertEqual(8, result)

    def test_calculate_subtraction_two_negative_numbers(self):
        test_text = "-9 - 1"
        result = self.logic.calculate(test_text)
        self.assertEqual(-10, result)

    def test_getLastExpressionWhenHistoryIsEmpty(self):
        with self.assertRaises(LogicException) as le:
            self.logic.get_last_expression()
        self.assertEqual("History is empty!", str(le.exception))

    def test_getLastExpressionAfterComputation(self):
        test_text = "-128 - 256"
        self.logic.calculate(test_text)
        history_item = self.logic.get_last_expression()
        self.assertEqual(test_text, history_item)

    def test_getLastExpressionAfterTwoCalculates(self):
        test_text = "-128 - 256"
        test_text_2 = "8*8"
        self.logic.calculate(test_text)
        self.logic.calculate(test_text_2)

        history_item = self.logic.get_last_expression()
        self.assertEqual(test_text_2, history_item)

        history_item = self.logic.get_last_expression()
        self.assertEqual("-384", history_item)

        history_item = self.logic.get_last_expression()
        self.assertEqual(test_text, history_item)

        with self.assertRaises(LogicException) as le:
            self.logic.get_last_expression()
        self.assertEqual("History is empty!", str(le.exception))


if "__main__" == __name__:
    unittest.main()
