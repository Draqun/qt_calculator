# QtCalculator [![pipeline status](https://gitlab.com/Draqun/qt_calculator/badges/master/pipeline.svg)](https://gitlab.com/Draqun/qt_calculator/-/commits/master) [![coverage report](https://gitlab.com/Draqun/qt_calculator/badges/master/coverage.svg)](https://gitlab.com/Draqun/qt_calculator/-/commits/master)

Application is a simple example how to create application in PyQt5

### Author
Damian Giebas

damian.giebas@gmail.com
